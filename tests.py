import itertools

import pytest

import rere


def test_reproduce():
    for comment, html in [
            ("Simplest", "<html></html>"),
            ("Attribute", """<html lang="en"></html>"""),
            ("Nest", "<html><body></body></html>"),
            (
                "Indent",
                """
                <html>
                    <body></body>
                </html>
                """
            ),
            ("Same level elements on the top level", "<b>Hello</b><i>world</i>"),
            ("Children", "<html><head></head><body></body></html>"),
            ("Doctype", "<!DOCTYPE html><html></html>"),
            ("Self closed", "Hello<i />World"),
            ("Comment", "Hello<!-- comment -->World"),
            ("CDATA", "<script><![CDATA[ <HTML> ]]></script>"),
            ("Non starttag", "Hello</el>"),
            ("Data after element", "<html>a<head>b<link />c</head>d</html>"),
            ("XML declaration", """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
Hello World
</html>""")
    ]:
        actual = rere.parse_markup(html).to_string()
        assert actual == html, "{}: {}".format(comment, actual)


def atest_actuall_website():
    for comment, filename in [
            ("Gengo", "gengo.html"),
            ("Wikipedia", "wikipedia.html"),
            ("Python", "python.html"),
    ]:
        with open(filename) as f:
            html = f.read()
        actual = rere.parse_markup(html).to_string()
        assert actual == html, "{}: {}".format(comment, actual)


def test_translate():
    document = rere.parse_markup("<em>Hello</em>")
    assert document.tag.text == "Hello"
    document.tag.text = "こんにちは"
    assert document.to_string() == "<em>こんにちは</em>"


def test_childrean():
    document = rere.parse_markup("<div><em>Hello</em><em>World</em></div>")
    for actual, expected in itertools.zip_longest(document.tag.children, ["Hello", "World"]):
        assert actual.text == expected


def test_plaintext():
    assert rere.parse_markup("Hello").head == "Hello"


def test_tag_name():
    document = rere.parse_markup("<img/>")
    assert document.tag.name == 'img'


@pytest.mark.html_escape
def test_html_escape():
    document = rere.parse_markup("<html>&amp;</html>&amp;")
    assert document.tag.text == '&'
    document.tag.text == '&'
    assert document.to_string() == "<html>&amp;</html>&amp;"


def test_comment():
    source = "<em><!-- comment </em> text -->comment tail</em>"
    document = rere.parse_markup(source)
    em = document.tag
    assert em.name == 'em'
    assert em.text == ""
    assert len(em.children) == 1
    comment = em.children[0]
    assert comment.name == '#comment#'
    assert comment.text == " comment </em> text "


def test_doctype():
    source = "Hello<!DOCTYPE html>HTML<html>Parser</html>"
    document = rere.parse_markup(source)
    assert document.head == "Hello"
    doctype = document.tag
    assert doctype.name == "#DOCTYPE#"
    assert doctype.starttag == "<!DOCTYPE html>"
    assert doctype.endtag is None
    assert doctype.is_self_closed
    html = doctype.next
    assert html.name == "html"


def test_stray():
    source = """<header id="1"></header><div id="2"></header></div>"""
    document = rere.parse_markup(source)
    header1 = document.tag
    assert header1.name == 'header'
    div = header1.next
    assert div.name == 'div'
    stray_header = div.children[0]
    assert stray_header.name == 'header'
    assert stray_header.is_self_closed
