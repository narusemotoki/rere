#!/usr/bin/env python3
import setuptools

setuptools.setup(
    name='rere',
    version='0.2.4',
    classifiers=[
        'Programming Language :: Python',
    ],
    author='Motoki Naruse',
    author_email='motoki@naru.se',
    url="https://bitbucket.org/narusemotoki/rere",
    py_modules=["rere"]
)
