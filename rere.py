import abc
import html
import html.parser
from typing import (
    Dict,
    Optional,
)


class ElementBase(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def to_string(self) -> str:
        ...

    @abc.abstractmethod
    def is_closed(self) -> bool:
        ...


class Element(ElementBase):
    __slots__ = [
        'name', 'starttag', 'endtag', 'parent', 'children', '_text', '_tail', 'next', 'previous',
        'is_self_closed'
    ]

    def __init__(
            self, name: str, *, starttag: str=None, endtag: str=None, is_self_closed=False
    ) -> None:
        self.name = name
        self.starttag = starttag
        self.endtag = endtag
        self.parent = None
        self.children = []
        self._text = ''
        self._tail = ''
        self.next = None
        self.previous = None
        self.is_self_closed = is_self_closed

    @property
    def tail(self) -> str:
        return html.unescape(self._tail)

    @tail.setter
    def tail(self, value: str) -> None:
        self._tail = html.escape(value)

    @property
    def text(self) -> str:
        return html.unescape(self._text)

    @text.setter
    def text(self, value: str) -> None:
        self._text = html.escape(value)

    def is_closed(self) -> bool:
        return bool(self.endtag) or self.is_self_closed

    def to_string(self) -> str:
        return "".join(
            [
                self.starttag or "",
                self._text,
                self.children[0].to_string() if self.children else "",
                self.endtag or "",
                self._tail,
                self.next.to_string() if self.next else ""
            ]
        )

    def __repr__(self) -> str:
        return "{}: {}{}{}".format(self.name, self.starttag, len(self.children), self.endtag)


class MarkupParser(html.parser.HTMLParser):
    def __init__(self) -> None:
        self.non_closed_elements = []
        self.head = ''
        self.last_tag = None
        self.__starttag_text = None
        super().__init__(convert_charrefs=True)

    def get_starttag_text(self):
        """Return full source of start tag: '<...>'."""
        return self.__starttag_text

    def _struct_element(self, element: ElementBase) -> None:
        if self.last_tag:
            if self.last_tag.is_closed():
                if self.last_tag.parent:
                    element.parent = self.last_tag.parent
                    element.parent.children.append(element)
                self.last_tag.next, element.previous = element, self.last_tag
            else:
                element.parent = self.last_tag
                self.last_tag.children.append(element)
        self.last_tag = element

    def parse_starttag(self, i):
        self.__starttag_text = None
        endpos = self.check_for_whole_start_tag(i)
        if endpos < 0:
            return endpos
        rawdata = self.rawdata
        self.__starttag_text = rawdata[i:endpos]

        # Now parse the data between i+1 and j into a tag and attrs
        attrs = []
        match = html.parser.tagfind_tolerant.match(rawdata, i+1)
        assert match, 'unexpected call to parse_starttag()'
        k = match.end()
        self.lasttag = tag = match.group(1).lower()
        while k < endpos:
            m = html.parser.attrfind_tolerant.match(rawdata, k)
            if not m:
                break
            attrname, rest, attrvalue = m.group(1, 2, 3)
            if not rest:
                attrvalue = None
            elif attrvalue[:1] == '\'' == attrvalue[-1:] or attrvalue[:1] == '"' == attrvalue[-1:]:
                attrvalue = attrvalue[1:-1]
            if attrvalue:
                attrvalue = html.parser.unescape(attrvalue)
            attrs.append((attrname.lower(), attrvalue))
            k = m.end()

        end = rawdata[k:endpos].strip()
        if end not in (">", "/>"):
            lineno, offset = self.getpos()
            if "\n" in self.__starttag_text:
                lineno = lineno + self.__starttag_text.count("\n")
                offset = len(self.__starttag_text) - self.__starttag_text.rfind("\n")
            else:
                offset = offset + len(self.__starttag_text)
            self.handle_data(rawdata[i:endpos])
            return endpos
        if end.endswith('/>'):
            # XHTML-style empty tag: <span attr="value" />
            self.handle_startendtag(tag, attrs)
        else:
            self.handle_starttag(tag, attrs)
            if tag in self.CDATA_CONTENT_ELEMENTS:
                self.set_cdata_mode(tag)
        return endpos

    def parse_endtag(self, i):
        rawdata = self.rawdata
        assert rawdata[i:i+2] == "</", "unexpected call to parse_endtag"
        match = html.parser.endendtag.search(rawdata, i+1)  # >
        if not match:
            return -1
        gtpos = match.end()
        match = html.parser.endtagfind.match(rawdata, i)  # </ + tag + >
        if not match:
            if self.cdata_elem is not None:
                self.handle_data(rawdata[i:gtpos])
                return gtpos
            # find the name: w3.org/TR/html5/tokenization.html#tag-name-state
            namematch = html.parser.tagfind_tolerant.match(rawdata, i+2)
            if not namematch:
                # w3.org/TR/html5/tokenization.html#end-tag-open-state
                if rawdata[i:i+3] == '</>':
                    return i+3
                else:
                    return self.parse_bogus_comment(i)
            tagname = namematch.group(1)
            # consume and ignore other stuff between the name and the >
            # Note: this is not 100% correct, since we might have things like
            # </tag attr=">">, but looking for > after tha name should cover
            # most of the cases and is much simpler
            gtpos = rawdata.find('>', namematch.end())
            self.handle_endtag(tagname, namematch.group())
            return gtpos+1

        elem = match.group(1).lower()  # script or style
        if self.cdata_elem is not None:
            if elem != self.cdata_elem:
                self.handle_data(rawdata[i:gtpos])
                return gtpos

        self.handle_endtag(elem, match.group())
        self.clear_cdata_mode()
        return gtpos

    def create_element(self, tagname, **kwargs) -> Element:
        if tagname.lower() in self.CDATA_CONTENT_ELEMENTS:
            return CDataElement(tagname, **kwargs)
        return Element(tagname, **kwargs)

    def handle_decl(self, decl: str) -> None:
        self._struct_element(Doctype("<!{}>".format(decl)))

    def handle_starttag(self, tagname: str, attrs: Dict[str, str]) -> None:
        element = self.create_element(tagname, starttag=self.get_starttag_text())
        self._struct_element(element)
        self.non_closed_elements.append(element)

    def handle_endtag(self, tagname: str, tag: str) -> None:
        """If the tag is in non_closed_elements, remove whole tags from the list until hit the
        element.
        If the tag is not in non_closed_elements, the tag is handled as self_closed tag.
        """
        has_starttag = any(element.name == tagname for element in self.non_closed_elements)
        while has_starttag:
            last_element = self.non_closed_elements.pop()
            if last_element.name == tagname:
                last_element.endtag = tag
                self.last_tag = last_element
                return
        self._struct_element(self.create_element(tagname, starttag=tag, is_self_closed=True))

    def handle_startendtag(self, name: str, attrs: Dict[str, str]) -> None:
        self._struct_element(
            self.create_element(name, starttag=self.get_starttag_text(), is_self_closed=True))

    def handle_data(self, data: str) -> None:
        if self.last_tag:
            if self.last_tag.is_closed():
                self.last_tag.tail = data
            else:
                self.last_tag.text = data
        else:
            self.head += data

    def handle_comment(self, data) -> None:
        comment = Comment(starttag="<!--{}-->".format(data))
        comment.text = data
        self._struct_element(comment)

    def handle_pi(self, data: str) -> None:
        self._struct_element(ProcessingInstruction(starttag="<?{}>".format(data)))


class Comment(Element):
    def __init__(self, starttag: str) -> None:
        super().__init__("#comment#", starttag=starttag, is_self_closed=True)

    @property
    def tail(self) -> str:
        return html.unescape(self._tail)

    @tail.setter
    def tail(self, value: str) -> None:
        self._tail = html.escape(value)

    def to_string(self) -> str:
        return "".join(
            [
                self.starttag,
                self._tail,
                self.next.to_string() if self.next else ""
            ]
        )

    def is_closed(self) -> bool:
        return True

    @classmethod
    def is_comment(cls, tag: str) -> bool:
        return tag.startswith("<!--") and tag.endswith("-->")


class CDataElement(Element):
    @property
    def text(self) -> str:
        return self._text

    @text.setter
    def text(self, value: str) -> None:
        self._text = value


class Doctype(Element):
    def __init__(self, starttag: str) -> None:
        super().__init__("#DOCTYPE#", starttag=starttag, is_self_closed=True)


class ProcessingInstruction(Element):
    def __init__(self, starttag: str) -> None:
        super().__init__("#ProcessingInstruction#", starttag=starttag, is_self_closed=True)


class Document(ElementBase):
    __slots__ = ['tag', 'children', '_text']

    def __init__(self, head: str, tag: Optional[ElementBase]) -> None:
        self.tag = tag
        self._text = head
        self.children = [tag]

    @property
    def head(self) -> str:
        return self.text

    @head.setter
    def head(self, value: str) -> None:
        self._text = value

    @property
    def text(self) -> str:
        return html.unescape(self._text)

    @text.setter
    def text(self, value: str) -> None:
        self._text = html.escape(value)

    def to_string(self) -> str:
        return "".join(
            [
                self._text,
                self.children[0].to_string() if self.children else "",
            ]
        )

    def is_closed(self) -> bool:
        return True


def _find_top_element(element: ElementBase) -> ElementBase:
    while True:
        if element.parent is not None:
            element = element.parent
            continue
        if element.previous is not None:
            element = element.previous
            continue
        return element


def parse_markup(markup_text: str) -> Document:
    parser = MarkupParser()
    parser.feed(markup_text)
    if parser.last_tag:
        top_element = _find_top_element(parser.last_tag)
    else:
        top_element = None

    return Document(parser.head, top_element)


HTMLParser = MarkupParser


def parser_html(html: str) -> Document:
    return parse_markup(html)
